# Beheer Referentie Regels

Verkenning CMS functionaliteit om regels in Markdown-content voor een SSG[^1] te beheren.

| Service | Locatie |
|:-|:-|
| Content Management | [![Netlify CMS](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Netlify_CMS_logo.svg/96px-Netlify_CMS_logo.svg.png)](https://istd-regels.netlify.app/admin) |
| Preview Site Beheer| [![Netlify Status](https://api.netlify.com/api/v1/badges/e9e57b59-6b2f-428d-ae7b-069202e646c6/deploy-status)](https://app.netlify.com/sites/istd-regels/deploys) |
| Preview Site | https://istd-regels.netlify.app |
| Publicatie Site | [GitLab Pages](https://istddevops.gitlab.io/exploration/regels/beheer-referentie-regels/docs/restricties/rs001-ref) |

[^1]: **SSG** staat hiet voor **S**tatische **S**ite **G**enerator
