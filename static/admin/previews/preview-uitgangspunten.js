/**
 * Netifly CMS: iStandaard Regels Uitgnagspunten Preview
 */
var previewUitgangspunten = createClass({
    render: function() {
      var entry = this.props.entry;
      return h('article', {"class": "markdown"},
        h('strong', {}, entry.getIn(['data', 'regelCode'])),
        h('blockquote', {"class": "book-hint info"}, entry.getIn(['data', 'description'])),
        h('div', {}, entry.getIn(['data', 'wordtGebruiktIn'])),
        h('div', {"className": "text"}, this.props.widgetFor('body'))
      );
    }
  });

  CMS.registerPreviewTemplate("uitgangspunten", previewUitgangspunten);