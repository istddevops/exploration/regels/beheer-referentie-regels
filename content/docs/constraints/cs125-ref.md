---
bookToC: false
regelType: Constraints
regelCode: CS125-ref
description: Vullen met '1' (eerste aanlevering), '2' (wijzigen aanlevering) of '3' (verwijderen aanlevering).
wordtGebruiktIn: iEb 1.0, iWlz 2.3, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
