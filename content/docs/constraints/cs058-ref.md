---
bookToC: false
regelType: Constraints
regelCode: CS058-ref
description: 1 (eerste aanlevering) of 3 (verwijderen aanlevering) vullen.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
