---
bookToC: false
regelType: Constraints
regelCode: CS136-ref
description: Een einddatum of mutatiedatum van een zorglevering moet groter dan of gelijk zijn aan de begindatum van de betreffende levering.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(NB: deze regel had regelcode TR018 in de releases iWlz 2.2, iWmo 3.0 en iJw 3.0)
  
{{< /hint >}}
