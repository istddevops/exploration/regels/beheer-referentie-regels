---
bookToC: false
regelType: Restricties
regelCode: RS025-ref
description: Maximale lengte 64 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iJw 3.0, iJw 3.1
XSDString: 'maxLength value="64"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
