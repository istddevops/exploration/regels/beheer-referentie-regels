---
bookToC: false
regelType: Restricties
regelCode: RS046-ref
description: Aaneengesloten vullen met cijfers.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'pattern value="[0-9]*"'
---
{{< hint warning >}}
  
(NB: deze regel had regelcode CS005 in de releases iWlz 2.2, iJw 3.0, iWmo 3.0 en iPgb 2.0)
  
{{< /hint >}}
