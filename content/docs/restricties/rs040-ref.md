---
bookToC: false
regelType: Restricties
regelCode: RS040-ref
description: Maximale waarde 999999999999 (12*9)
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iJw 3.0, iJw 3.1
XSDString: 'maxInclusive value="999999999999"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
