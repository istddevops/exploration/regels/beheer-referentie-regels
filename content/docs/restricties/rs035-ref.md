---
bookToC: false
regelType: Restricties
regelCode: RS035-ref
description: Vullen met 4 cijfers.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iPgb 2.0, iJw 3.0, iJw 3.1
XSDString: 'pattern value="[0-9]{4}"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
