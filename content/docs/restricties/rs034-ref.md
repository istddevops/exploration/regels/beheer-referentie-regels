---
bookToC: false
regelType: Restricties
regelCode: RS034-ref
description: Tijd vullen zonder tijdszone.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'pattern value ="[^Z+-]+"'
---
{{< hint warning >}}
  
Bij datatypen van het type time is het niet toegestaan om een tijdzone mee te geven. Alleen HH:MM:SS (en optioneel miliseconden .mmm) is toegestaan
Bijvoorbeeld:
13:59:41 of 13.59.41.123 is toegestaan
13.59.41+1 is niet toegestaan
  
{{< /hint >}}
