---
bookToC: false
regelType: Restricties
regelCode: RS004-ref
description: Maximale waarde 99999 (5*9)
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxInclusive value="99999"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
