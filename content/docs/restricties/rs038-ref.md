---
bookToC: false
regelType: Restricties
regelCode: RS038-ref
description: Vullen met UUID versie 4
wordtGebruiktIn: iWlz 2.3, iWlz 2.2
XSDString: 'pattern value="[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
