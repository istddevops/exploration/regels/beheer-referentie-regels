---
bookToC: false
regelType: Restricties
regelCode: RS023-ref
description: Maximale lengte 35 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxLength value="35"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
