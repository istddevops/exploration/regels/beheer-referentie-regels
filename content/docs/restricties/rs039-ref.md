---
bookToC: false
regelType: Restricties
regelCode: RS039-ref
description: Maximale lengte 14 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iJw 3.0, iJw 3.1
XSDString: 'maxLength value="14"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
