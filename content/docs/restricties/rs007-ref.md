---
bookToC: false
regelType: Restricties
regelCode: RS007-ref
description: Minimale lengte 1 positie
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'minLength value="1"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
