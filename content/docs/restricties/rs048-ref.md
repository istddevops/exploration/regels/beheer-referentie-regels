---
bookToC: false
regelType: Restricties
regelCode: RS048-ref
description: Vullen met een versienummer bestaande uit drie gehele getallen, gescheiden met punten.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'pattern value="(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)" '
---
{{< hint warning >}}
  
(NB: deze regel had regelcode CS128 in de releases iWlz 2.2, iJw 3.0, iWmo 3.0 en iEb 1.0)
  
{{< /hint >}}
