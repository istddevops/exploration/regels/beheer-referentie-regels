---
bookToC: false
regelType: Restricties
regelCode: RS012-ref
description: Maximale lengte 5 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iJw 3.0, iJw 3.1
XSDString: 'maxLength value="5"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
