---
bookToC: false
regelType: Restricties
regelCode: RS037-ref
description: Vullen met 9 cijfers.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'pattern value="[0-9]{9}"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
