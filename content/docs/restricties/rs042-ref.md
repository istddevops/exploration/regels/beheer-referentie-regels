---
bookToC: false
regelType: Restricties
regelCode: RS042-ref
description: Minimale waarde 1
wordtGebruiktIn: iWlz 2.3, iWlz 2.2
XSDString: 'minInclusive value="1"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
