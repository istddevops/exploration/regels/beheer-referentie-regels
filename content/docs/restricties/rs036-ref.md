---
bookToC: false
regelType: Restricties
regelCode: RS036-ref
description: Vullen met 8 cijfers.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'pattern value="[0-9]{8}"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
