---
bookToC: false
regelType: Restricties
regelCode: RS045-ref
description: Aaneengesloten vullen (zonder punten of spaties).
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'pattern value="([a-zA-ZÀ-ỳ])+"'
---
{{< hint warning >}}
  
(NB: deze regel had regelcode CS004 in de releases iWlz 2.2, iJw 3.0, iWmo 3.0 en iPgb 2.0)
  
{{< /hint >}}
