---
bookToC: false
regelType: Restricties
regelCode: RS005-ref
description: Maximale waarde 99999999 (8*9)
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iPgb 2.0, iJw 3.0, iJw 3.1
XSDString: 'maxInclusive value="99999999"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
