---
bookToC: false
regelType: Restricties
regelCode: RS006-ref
description: Maximale waarde 999999999 (9*9)
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxInclusive value="999999999"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
