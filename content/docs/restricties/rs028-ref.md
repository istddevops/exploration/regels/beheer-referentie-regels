---
bookToC: false
regelType: Restricties
regelCode: RS028-ref
description: Maximale lengte 200 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxLength value="200"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
