---
bookToC: false
regelType: Restricties
regelCode: RS024-ref
description: Maximale lengte 36 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxLength value="36"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
