---
bookToC: false
regelType: Restricties
regelCode: RS013-ref
description: Maximale lengte 6 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxLength value="6"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
