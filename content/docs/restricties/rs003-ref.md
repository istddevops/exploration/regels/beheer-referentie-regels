---
bookToC: false
regelType: Restricties
regelCode: RS003-ref
description: Maximale waarde 9999
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxInclusive value="9999"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
