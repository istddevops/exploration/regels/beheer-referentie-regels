---
bookToC: false
regelType: Restricties
regelCode: RS018-ref
description: Maximale lengte 15 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxLength value="15"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
