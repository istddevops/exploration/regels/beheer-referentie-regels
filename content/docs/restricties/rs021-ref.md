---
bookToC: false
regelType: Restricties
regelCode: RS021-ref
description: Maximale lengte 25 posities
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxLength value="25"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
