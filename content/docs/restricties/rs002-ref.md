---
bookToC: false
regelType: Restricties
regelCode: RS002-ref
description: Maximale waarde 99
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
XSDString: 'maxInclusive value="99"'
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
