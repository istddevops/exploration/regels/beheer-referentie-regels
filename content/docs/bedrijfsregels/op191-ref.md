---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP191-ref
description: Het gebruik van ongestructureerde informatie dient tot een minimum beperkt te worden.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
In sommige berichten kan een commentaarveld gebruikt worden om extra informatie op te nemen. Aanvullende regels hiervoor kunnen per iStandaard vastgelegd worden.
  
{{< /hint >}}
