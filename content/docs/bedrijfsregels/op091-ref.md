---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP091-ref
description: Van een client mogen aanvullende contactgegevens vastgelegd worden; er moet dan wel vastgelegd worden wat voor soort adres het betreft.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
