---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP035-ref
description: Mutaties in de zorglevering worden alleen doorgegeven aan het CAK wanneer deze van invloed kunnen zijn op de eigen bijdrage.
wordtGebruiktIn: iEb 1.0, iWlz 2.3, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
