---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP087-ref
description: In een toewijzingbericht moet een compleet beeld van alle actuele toewijzingen worden opgenomen.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
Een actuele toewijzing is een toewijzing die op of na de aanmaakdatum van het bericht geldig is.
  
{{< /hint >}}
