---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP272-ref
description: De aanbieder verzendt een stopbericht binnen vijf werkdagen na de daadwerkelijke datum waarop de ondersteuning beeindigd is.
wordtGebruiktIn: iWmo 3.1, iJw 3.1
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
