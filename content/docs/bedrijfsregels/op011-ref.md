---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP011-ref
description: Begindatum van geleverde zorg of ondersteuning wordt gemeld nadat de levering gestart is.
wordtGebruiktIn: iWmo 3.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
Test datum zou 9 dec moeten zijn
  
{{< /hint >}}
