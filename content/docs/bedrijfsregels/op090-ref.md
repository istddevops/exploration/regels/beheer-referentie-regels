---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP090-ref
description: Voor ieder ontvangen bericht moet binnen een afgesproken periode na ontvangst een retourbericht verzonden worden.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
Per iStandaard worden afspraken vastgelegd over de reactietermijn waarbinnen een ontvanger een retourbericht moet sturen aan de verzender.
  
{{< /hint >}}
