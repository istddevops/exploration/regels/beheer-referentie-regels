---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP071-ref
description: Elke relatie moet een nummer krijgen om hem uniek te identificeren.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
Per instantie die dit nummer voor het berichtenverkeer vaststelt moet dit nummer uniek zijn per client. Het nummer mag niet gewijzigd worden.
  
{{< /hint >}}
