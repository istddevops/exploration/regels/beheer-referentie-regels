---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP079-ref
description: Het is verplicht om gebruik te maken van het BSN van de client in de onderlinge uitwisseling van gegevens.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
