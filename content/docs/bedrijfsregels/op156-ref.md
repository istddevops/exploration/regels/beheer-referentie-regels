---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP156-ref
description: De instantie die verantwoordelijk is voor de inzet van zorg of ondersteuning voor de client is verantwoordelijk voor het aanleveren van gegevens aan het CAK over de start en de beeindiging van een levering van zorg of ondersteuning.
wordtGebruiktIn: iEb 1.0, iWlz 2.3, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
Voor de Wlz is dit het regionale zorgkantoor, voor de Wmo is dit de gemeente.
  
{{< /hint >}}
