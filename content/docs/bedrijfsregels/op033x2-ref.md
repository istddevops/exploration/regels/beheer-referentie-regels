---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP033x2-ref
description: Het gebruik van status aanlevering met een waarde 2 (een berichtklasse is gewijzigd) is alleen in specifieke situaties toegestaan.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
Wijzigingen en correcties kunnen daarom niet altijd in een bericht worden doorgegeven.
  
{{< /hint >}}
