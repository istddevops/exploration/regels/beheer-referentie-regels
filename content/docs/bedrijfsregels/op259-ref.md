---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP259-ref
description: Het is niet toegestaan om een zorg- of ondersteuningsproduct gestapeld toe te wijzen
wordtGebruiktIn: iWmo 3.1, iJw 3.1
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
