---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP347-ref
description: Het is niet toegestaan om een zorg- of ondersteuningsproduct gestapeld aan te vragen
wordtGebruiktIn: iWmo 3.1, iJw 3.1
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
