---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP072-ref
description: Een melding over de beeindiging van een zorglevering kan alleen gedaan worden indien eerder de start van diezelfde levering gemeld is.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
geenTechnischeRegels: false
---
{{< hint warning >}}
  
In het bericht moet gerefereerd worden aan de start van de levering.
  
{{< /hint >}}
