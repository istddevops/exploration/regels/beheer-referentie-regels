---
bookToC: false
regelType: Bedrijfsregels
regelCode: OP271-ref
description: De aanbieder verzendt een startbericht binnen vijf werkdagen na de daadwerkelijke datum waarop de ondersteuning gestart is
wordtGebruiktIn: iWmo 3.1, iJw 3.1
---
{{< hint warning >}}
  
Indien de ondersteuning met terugwerkende kracht is toegewezen, binnen vijf werkdagen na ontvangst van het toewijzingbericht.
  
{{< /hint >}}
