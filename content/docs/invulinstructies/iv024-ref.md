---
bookToC: false
regelType: Invulinstructies
regelCode: IV024-ref
description: Hoe moeten bedragen worden gevuld?
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iPgb 2.0, iJw 3.0, iJw 3.1
---
{{< hint warning >}}
  
Bedragen worden in 1/100 van de gehanteerde valuta opgenomen. Als er sprake is van de euro (EUR), dan is als voorbeeld 10000 gelijk aan tienduizend eurocent oftewel 100 euro.
  
{{< /hint >}}
