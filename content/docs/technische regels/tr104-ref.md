---
bookToC: false
regelType: Technische regels
regelCode: TR104-ref
description: Een verwijderd bericht kan niet gewijzigd worden.
wordtGebruiktIn: iEb 1.0, iWlz 2.3, iWlz 2.2
---
{{< hint warning >}}
  
Een bericht dat eerder met status aanlevering '3' (Verwijderen aanlevering) verwijderd is, kan niet gewijzigd worden met status aanlevering '2' (Gewijzigde aanlevering). 
  
{{< /hint >}}
