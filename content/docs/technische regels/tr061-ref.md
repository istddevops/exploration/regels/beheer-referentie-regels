---
bookToC: false
regelType: Technische regels
regelCode: TR061-ref
description: Bij een client moet een adres voorkomen dat gebruikt kan worden voor correspondentie met de client.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
