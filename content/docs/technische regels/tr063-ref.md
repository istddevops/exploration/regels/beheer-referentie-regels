---
bookToC: false
regelType: Technische regels
regelCode: TR063-ref
description: Indien StatusAanlevering de waarde 3 (aanlevering verwijderen) bevat, dan moet voor de betreffende Client een eerdere aanlevering met dezelfde sleutel verstuurd zijn.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
rationale: Met status aanlevering 3 wordt aangegeven dat de eerder verzonden berichtklasse verkeerde informatie bevatte en daarom verwijderd moet worden. De sleutelvelden van de te verwijderen berichtklasse moeten overeenkomen met de eerder verzonden berichtklasse om te kunnen bepalen welke berichtklasse verwijderd moet worden.
---
{{< hint warning >}}
  
Gezien het corrigerende karakter van een aanlevering met StatusAanlevering 3, is het niet nodig om hierop overige inhoudelijke controles uit te voeren.  

Opmerking: Deze regel is bedoeld om het gebruik van de waarde 3 in goede banen te leiden en zegt niets over het mogelijk toegestaan zijn van deze waarde. Eventuele beperkingen van dit gebruik worden via een constraint (of eventueel andere technische regels) beschreven.
  
{{< /hint >}}
