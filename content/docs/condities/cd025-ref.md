---
bookToC: false
regelType: Condities
regelCode: CD025-ref
description: Als Adres / LandCode de waarde NL (Nederland) heeft, dan verplicht vullen.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iPgb 2.0, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
