---
bookToC: false
regelType: Condities
regelCode: CD007-ref
description: Als Communicatie / Vorm de waarde 1 (tolk taal) heeft, dan verplicht vullen, anders leeglaten.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
