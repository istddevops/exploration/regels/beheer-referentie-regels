---
bookToC: false
regelType: Uitgangspunten
regelCode: UP017-ref
description: Informatieuitwisseling volgens de iStandaarden is gebaseerd op gestandaardiseerd berichtenverkeer.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
