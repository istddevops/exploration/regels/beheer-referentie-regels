---
bookToC: false
regelType: Uitgangspunten
regelCode: UP025-ref
description: De geleverde zorg of ondersteuning wordt gedeclareerd op clientniveau.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
