---
bookToC: false
regelType: Uitgangspunten
regelCode: UP001-ref
description: De iStandaarden ondersteunen het administratieve cliëntproces dat
  noodzakelijk is voor de uitvoering van de Wlz, de Wmo en de Jeugdwet.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}

(geen documentatie)

{{< /hint >}}