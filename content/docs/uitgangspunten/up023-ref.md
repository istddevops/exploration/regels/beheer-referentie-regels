---
bookToC: false
regelType: Uitgangspunten
regelCode: UP023-ref
description: Informatie wordt eenmalig bij de client uitgevraagd.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iEb 1.0, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
