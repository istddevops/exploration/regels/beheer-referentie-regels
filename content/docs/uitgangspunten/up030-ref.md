---
bookToC: false
regelType: Uitgangspunten
regelCode: UP030-ref
description: BRP is bron voor persoonsgegevens.
wordtGebruiktIn: iPgb 2.0
---
{{< hint warning >}}
  
(NB: deze regel had regelcode UP029 in de release iPgb 2.0)

  
{{< /hint >}}
