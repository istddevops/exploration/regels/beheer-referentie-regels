---
bookToC: false
regelType: Uitgangspunten
regelCode: UP016-ref
description: Per iStandaard worden verschillende soorten zorg of ondersteuning  uitgedrukt volgens een vastgestelde codelijst.
wordtGebruiktIn: iWmo 3.0, iWmo 3.1, iWlz 2.3, iJw 3.0, iJw 3.1, iWlz 2.2
---
{{< hint warning >}}
  
(geen documentatie)
  
{{< /hint >}}
